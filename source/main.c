#include <stdio.h>
#include "dice.h"

int main() {
	initializeSeed();
	int faces;
	//print  initial message
	printf("Choose the number of faces\n");
	scanf("%d", &faces);
	printf("Let's roll the dice: %d\n", rollDice(faces));
	return 0;
}

